<?php
    error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
    require_once __DIR__.'/connect.php';
    require_once __DIR__.'/update.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Обновление количества товаров</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <br>
            <div class="alert alert-warning" role="alert">
                <strong>Для корректной работы необходимо выполнить следующие условия:</strong>
                <ul>
                    <li>Поддерживаемые форматы: 'xls' и 'xlsx';</li>
                    <li>Обязательное присутствие вкладки 'Склад Рязани' в документе;</li>
                    <li>Обязательное присутствие столбца 'Модель'(название не имеет значения) 2-м на вкладке(по нему
                        происходит сравнение);
                    </li>
                    <li>Обязательное присутствие столбца 'Доступно'(название не имеет значения) 4-м на вкладке(из него
                        происходит выборка);
                    </li>
                    <li>Для разработки универсального скрипта обратитесь к <a href="https://t.me/sidorkevich">разработчику</a>;
                    </li>
                </ul>
            </div>
            <hr>
            <?php
                $db = new Connect;
                $pdo = $db->connectDB();
            ?>
            <form action="" class="form-group" method="post" enctype="multipart/form-data">
                <br><input type="file" class="form-control-file" id="exampleFormControlFile1" name="table" required>
                <br>
                <center>
                    <button type="submit" class="btn btn-primary mb-2">Обновить</button>
                </center>
            </form>
            <?php
                $obj = new Update();
                if (isset($_FILES['table'])) {
                    if (!$obj->uploadFile()) {
                        $text = "<strong>Ошибка загрузки файла!</strong><hr>note: Поддерживаемые типы файлов: 'xls' и 'xlsx'.";
                        $obj->getMessage('danger', $text);
                    } else {
                        $obj->updateCount($pdo);
                    }
                }
            ?>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</body>
</html>
