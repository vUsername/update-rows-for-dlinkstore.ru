<?php

    class DB
    {
        private $db_host = 'localhost';
        private $db_user = 'root';
        private $db_password = '';
        private $db_name = 'test';

        private function connectDb()//$db_host,$db_user,$db_password,$db_name
        {
            if ($pdo = new PDO('mysql:host=' . $this->db_host . ';dbname=' . $this->db_name,
                $this->db_user,
                $this->db_password,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION])) {
                return $pdo;
            } else {
                return false;
            }

        }

        public function updateBD($products)
        {
            $pdo = $this->connectDB();
            if ($pdo) {
                $count = 0;
                foreach ($products as $product) {
                    $qty = $product['qty'];
                    $model = $product['model'];
                    $query = "UPDATE oc_product SET quantity = $qty WHERE model = '$model'";
                    $count += $pdo->exec($query);
                }
                echo $count;
            }
        }

    }
