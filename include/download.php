<?php
    require '/../vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

    class Download
    {

        private $cookiePath = './cache/cookie.txt';
        private $toFile = './cache/temporary.xlsx';
        private $urlAuth = 'https://b2b.dlink.ru/esale/login/';
        private $urlDownload = 'https://b2b.dlink.ru/esale/get-file';
        private $username = 'jer-tcst';
        private $password = 'NPAmAKn87hahVDKk';
        private $sheetName = 'Склад Рязани';

        public function downloadFile()//$cookiePath
        {
            $cookieFile = new \GuzzleHttp\Cookie\FileCookieJar($this->cookiePath, true);
            $client = new \GuzzleHttp\Client([
                'http_errors ' => false,
                'headers' => [
                    'User-Agent' => $_SERVER['HTTP_USER_AGENT'],
                    'Content-Type' => 'multipart/form-data',
                    'Referer' => $this->urlAuth,
                ],
                'cookies' => $cookieFile,
            ]);

            $body = $client->get($this->urlAuth);
            $dom = new DOMDocument();
            $dom->loadHTML($body->getBody()->getContents());
            $xpath = new DOMXPath($dom);
            $form = $xpath->query('//form/input');
            $request_form = [];
            foreach ($form as $item) {
                $request_form[$item->getAttribute('name')] = $item->getAttribute('value');
            }
            $request_form['username'] = $this->username;
            $request_form['password'] = $this->password;
            try {
                $auth = $client->post($this->urlAuth, ['form_params' => $request_form]);
            } catch (Exception $e) {
            }

            $result = $client->get($this->urlDownload, ['save_to' => $this->toFile])->getBody()->getContents();

            if (!empty($result)) {
                return true;
            } else {
                return false;
            }
        }

        public function getArrayOfProducts()
        {

            if ($this->downloadFile()) {
                $reader = new Xlsx;
                $spreadsheet = $reader->load($this->toFile);
                $sheet = $spreadsheet->getSheetByName($this->sheetName);
                $highestRow = $sheet->getHighestRow();
                $products = array();
                $i = 1;
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $products[$i]['model'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
                    $products[$i]['qty'] = (int)$sheet->getCellByColumnAndRow('4', $row)->getValue();
                    $i++;
                }
                return $products;

            } else {
                return false;
            }
        }
    }