<?php
    require __DIR__.'/vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

    class Update
    {

        public $file_ext;
        public $file_tmp;


        public function __construct()
        {
            $this->file_ext = strtolower(end(explode('.', $_FILES['table']['name'])));
            $this->file_tmp = $_FILES['table']['tmp_name'];
        }

        public function getMessage($type, $text)
        {
            echo "
                                <div class=\"alert alert-$type alert-dismissible fade show\" role=\"alert\">
                                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                    <span aria-hidden=\"true\">&times;</span>
                                  </button>
                                  $text
                                </div>
                ";
        }

        public function uploadFile()
        {

            if (($this->file_ext == 'xls') || ($this->file_ext == 'xlsx')) {
                if (move_uploaded_file($this->file_tmp, "./upload/temp.$this->file_ext")) {
                    return true;
                } else
                    return false;
            }
        }


        public function getArrayOfProducts()
        {
            $reader = new Xlsx;
            $spreadsheet = $reader->load("./upload/temp.$this->file_ext");
            $sheet = $spreadsheet->getSheetByName('Склад Рязани');
            $highestRow = $sheet->getHighestRow();
            $products = array();
            $i = 1;
            for ($row = 2; $row <= $highestRow; ++$row) {
                $products[$i]['model'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
                $products[$i]['qty'] = (int)$sheet->getCellByColumnAndRow('4', $row)->getValue();
                $i++;
            }
            return $products;

        }

        public function updateCount($pdo)
        {
            $products = $this->getArrayOfProducts();
            try {
                $count = 0;
                foreach ($products as $product) {
                    $qty = $product['qty'];
                    $model = $product['model'];
                    $query = "UPDATE oc_product SET quantity = $qty WHERE model = '$model'";
                    $count += $pdo->exec($query);
                }
                $text = "<strong>Обновление базы прошло успешно!</strong> Изменено $count записей.";
                $this->getMessage('success', $text);

            } catch (PDOException $e) {
                echo $e->getMessage();
                $text = "Ошибка в запросе...";
                $this->getMessage('danger', $text);
            }

        }
    }