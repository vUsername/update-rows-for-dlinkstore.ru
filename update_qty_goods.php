#!/usr/bin/php
<?php
    /*
     * Этот скрипт должен быть завешен на cron.
     * Выполняет обновление количества товаров на складе из файла по ссылке.
     */

    @set_time_limit(0);
    ignore_user_abort(true);

    require("./include/download.php");

    $o = new Download();

    $products = $o->getArrayOfProducts();

    require("./include/db.php");

    $o = new Db();

    $o->updateBD($products);