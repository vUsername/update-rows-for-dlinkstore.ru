<?php

    class Connect
    {
        private $db_host = 'localhost';
        private $db_user = 'root';
        private $db_password = '';
        private $db_name = 'test';

        public function connectDb()
        {
            try {
                $pdo = new PDO('mysql:host=' . $this->db_host . ';dbname=' . $this->db_name, $this->db_user, $this->db_password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
                $text = "Подключение к бд: <strong>Успех!</strong>";
                $this->getMessage('success', $text);
                return $pdo;
            } catch (PDOException $e) {
                $text = "Подключение к бд: <strong>Отказ!</strong><hr>Проверьте конфигурацию...";
                $this->getMessage('danger', $text);
                return false;
            }
        }

        private function getMessage($type, $text)
        {
            echo "
                  <div class='alert alert-$type' role='alert'>
                  $text
                  </div>
                ";
        }
    }
